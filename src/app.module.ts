import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskController } from './tasks/task.controller';
import { Task } from './tasks/task.model';
import { TaskService } from './tasks/task.service';
import { TaskRepository } from './tasks/task.repository';

@Module({
  imports: [ TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'test',
    password: 'test',
    database: 'NoSQLDB',
    logging: true,
    synchronize: true,
    entities: [Task],
  })],
  controllers: [AppController, TaskController],
  providers: [AppService, TaskService, TaskRepository],
})
export class AppModule {}
