import { Injectable } from "@nestjs/common";
import { In } from "typeorm";
import { Task } from "./task.model";

@Injectable()
export class TaskRepository
{
async createTask(taskName:string, taskDescription:string)
{
    let newTask = new Task(taskName, taskDescription);
    await Task.getRepository().save(newTask);
}

async updateTask(oldName:string, newName:string, newDescription:string)
{
    let taskToUpdate = await Task.getRepository().findOne({ taskName: In([oldName])});
    taskToUpdate.taskName = newName;
    taskToUpdate.taskDescription = newDescription;
    await Task.getRepository().save(taskToUpdate);
}

async deleteTask(taskName:string)
{
    let taskToDelete = await Task.getRepository().findOne({ taskName: In([taskName])})
    await Task.getRepository().remove(taskToDelete);
}
}