import { Get, Post, Controller, Body, Put, Delete } from '@nestjs/common';
import { Task } from './task.model';
import { TaskService } from './task.service';

@Controller()
export class TaskController {
  constructor(private readonly service: TaskService) {}
  @Get('tasks')
  async findAll() {
    return await Task.find();
  }

  @Post('create')
  async create(@Body() body: any) {
    return this.service.createTask(body.taskName, body.taskDescription);
  }

  @Put('update')
  async update(@Body() body: any) {
    return this.service.updateTask(body.oldName, body.newName, body.newDescription);
  }

  @Delete('delete')
  async delete(@Body() body: any) {
    return this.service.deleteTask(body.taskName);
  }
}