import { Injectable } from "@nestjs/common";
import { TaskRepository } from "./task.repository";

@Injectable()
export class TaskService
{
    constructor(private readonly repository: TaskRepository) {}

async createTask(taskName:string, taskDescription:string)
{
  return this.repository.createTask(taskName, taskDescription);
}

async updateTask(oldName:string, newName:string, newDescription:string)
{
    return this.repository.updateTask(oldName, newName, newDescription);
}

async deleteTask(taskName:string)
{
    return this.repository.deleteTask(taskName);
}
}